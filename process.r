days <- 18
n_persons <- 3
data <- read.csv("_spread.csv")
data_new <- data

item_quantity <- c()
item_cost <- c()
reoccurring_bool <- c()
reoccurring <- data$Reoccurring
split <- data$Split

for (i in 1:nrow(data)) {
    quantity_row <- eval(parse(text = data$N[i]))
    cost_row <- eval(parse(text = data$Cost[i]))

    item_quantity <- c(item_quantity, quantity_row)
    item_cost <- c(item_cost, cost_row)

    if (data$Reoccurring[i] == 1) {
        reoccurring_bool <- c(reoccurring_bool, "YES")
    } else {
        reoccurring_bool <- c(reoccurring_bool, "NO")
    }
}

# item_quantity * item_cost (if reoccuring, multiply by number of days)
data_new$Total_Cost <- item_quantity * item_cost * (days - ((days - 1) * (1 - reoccurring)))
total_cost <- sum(data_new$Total_Cost)

# Sums all total_cost, but divide by # of people if splittable.
data_new$Per_Person <- data_new$Total_Cost / (n_persons - ((n_persons - 1) * (1 - split)))
per_person_cost <- sum(data_new$Per_Person)

money_format <- function (col) {
    format(round(col, 2), nsmall = 2)
}

data_new$Reoccurring <- reoccurring_bool
data_new$Cost <- money_format(data_new$Cost)

data_new <- rbind(data_new, list("Total", "", "", "", "", "", total_cost, per_person_cost))
data_new <- subset(data_new, select = -c(Split))

data_new$Per_Person <- money_format(data_new$Per_Person)
data_new$Total_Cost <- money_format(data_new$Total_Cost)

write.csv(data_new, "spread.csv", row.names=FALSE)
