
# Locations

(Approx. Time from Tokyo)

## Route

Note: Take the hotels with some leniency.
- Tokyo
    - [Hostel Hana An](https://www.hotelscombined.com/hotels/Hostel-Hana-An,Tokyo-p22200-h2620887-details/2023-06-25/2023-06-26/3adults#overview)

4-23-11 Kanamachi, Katsushika-ku, Tokyo 125-0042
- Utsunomiya (~1 hour from Tokyo)
    - [Smile Hotel Utsunomiya Higashiguchi](https://www.hotelscombined.com/hotels/Smile-Hotel-Utsunomiya-Higashiguchi,Utsunomiya-p22155-h657512-details/2023-06-25/2023-06-26/3adults?psid=duAkKyTbgN)

3-2-2 Higashishukugo, Utsunomiya 321-0953
- Fukushima
    - [The Celecton Fukushima](https://www.hotelscombined.com/hotels/The-Celecton-Fukushima,Fukushima-p23825-h415154-details/2023-06-25/2023-06-26/3adults?psid=duDkDOKtgp)

Otamachi 13-73, Fukushima 960-8068
- Sendai
    - [Hotel Livemax Sendai Kokubuncho](https://www.hotelscombined.com/hotels/Hotel-Livemax-Sendai-Kokubuncho,Sendai-p23750-h7358144-details/2023-06-25/2023-06-26/3adults?psid=duAkPFDGTy)

3-2-2 Higashishukugo, Utsunomiya 321-0953
- Ichinoseki
    - [Chisun Inn Iwate Ichinoseki Ic](https://www.hotelscombined.com/hotels/Chisun-Inn-Iwate-Ichinoseki-Ic,Ichinoseki-p23813-h416526-details/2023-06-25/2023-06-26/3adults?psid=dvAEK4qwAp)

188 2 Tsukimachi Akoogi Aza, Ichinoseki 021-0041
- Hachinohe
- Aomori
- Akita
- Niigata
- Ueno


## Other cities/locations

### North of Tokyo

- Nikko (~2 hours)

### South of Tokyo

- Koya-San (~6 hours)
- Tsumago (~5 hours)
- Nara (~4 hours)
- Kinosaki Onsen (~6 hours)


# Transportation


14 Day JR Pass might be alright, but buying individual tickets and planning
via [HyperDia].


# Items to have

WIP


## Essentials

-  Passport (duh)
    - Keep on you at all times. This is useful for if you get stopped, and tax-deductable purchases.
    - Also keep photo copies on you.
-  Cellphone (duh)
    - Get cheap sim card
-  Reusable water bottle
    - Preferably not too large (water is heavy)
-  Three days-ish worth of clothes
    - Don't pack too much clothes.
    - Laundry is cheap.
    - Clothes are big and heavy.



# Good to know

WIP


## Prep

-  Multiple copies of passport
-  Pack as lightly as possible (clothes for 3 days, nothing too heavy, ect.)
-  Get acclimated to a fuck ton of walking (5-7 km+ with backpack)


## During



# References

[https://thriftynomads.com/6-tips-to-travel-japan-on-the-cheap/](https://thriftynomads.com/6-tips-to-travel-japan-on-the-cheap/)

[https://thriftynomads.com/the-japan-rail-pass-worth-the-cost/](https://thriftynomads.com/the-japan-rail-pass-worth-the-cost/)

[https://www.nomadasaurus.com/budget-travel-in-japan/](https://www.nomadasaurus.com/budget-travel-in-japan/)

[https://www.nomadasaurus.com/hokkaido-itinerary/](https://www.nomadasaurus.com/hokkaido-itinerary/)

[https://www.neverendingvoyage.com/best-places-to-visit-in-japan/](https://www.neverendingvoyage.com/best-places-to-visit-in-japan/)

[Kosoku Bus](https://www.kosokubus.com/en/?ad=THR) - Book buses

[HotelsCombined](https://thriftynomads.com/go/hotelscombined) - Cheap Hotels

[https://japanrailpass.net/en/about_jrp.html](https://japanrailpass.net/en/about_jrp.html)

[https://www.japan-guide.com/e/e2359_001.html](https://www.japan-guide.com/e/e2359_001.html)

[https://japantravel.navitime.com/en/area/jp/route/](https://japantravel.navitime.com/en/area/jp/route/)

[HyperDia](https://www.hyperdia.com/) - Route Searching
