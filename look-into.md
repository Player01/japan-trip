
Please look into these things.

- [ ] Activities along route
- [ ] More information on withdrawing money
    - Exchange rate
    - Frequency of ATMs
    - How much money should be taken out at a time
- [ ] Length of time at each stop
- [ ] Number of stops
- [ ] List of travel items
- [ ] General travel knows
- [X] Pricing out JR Pass vs. individual tickets
- [X] Rules against 2 person rooms for 3 guests
- [X] Cheaper transportation in general
