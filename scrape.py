import subprocess

balls = "world"
print("hello {}".format(balls))

start = 17
min_days = 15
max_days = 19
for i in range(15):
    for j in range(max_days - min_days):
        from_month = 7
        from_day = start + i

        to_month = 7
        to_day = start + i + j + min_days

        if (to_day > 31):
            to_month = 8
            to_day = to_day - 31

        if (from_day > 31):
            from_month = 8
            from_day = from_day - 31
        subprocess.run(["firefox-developer-edition",
                        "https://www.kayak.com/flights/ORD-TYO/2023-{:02d}-{:02d}/2023-{:02d}-{:02d}/?sort=price_a".format(from_month, from_day, to_month, to_day)])
